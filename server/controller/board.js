var q = require('q'),
    _ = require('lodash');

module.exports = function(router, container) {
    var config = container.get('config');

    router.authenticated.get('/projects/:ns/:name/columns',
        function (req, res) {
            var timer = container.get('statsd').createTimer('redis.hgetall');

            q.all([
                q.ninvoke(container.get('redis'), 'hgetall', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns')
                    .then(function(columns) {
                        timer.stop();

                        return columns;
                    }),
                container.get('gitlab.labels').all(req.user.private_token, req.params.ns, req.params.name)
            ])
                .spread(function(columns, labels) {
                    return _.values(columns).map(function(column) {
                        var label;

                        column = JSON.parse(column);

                        if (labels) {
                            label = _.find(labels, { name: config.column_prefix + column.title.toLowerCase() });
                        }

                        if (!label) {
                            label = {
                                name: config.column_prefix + column.title.toLowerCase(),
                                color: '#E5E5E5'
                            };

                            container.get('gitlab.labels').persist(req.user.private_token, req.params.ns, req.params.name, label)
                                .then(function(label) {
                                    column.color = label.color;
                                });
                        } else {
                            column.color = label.color;
                        }

                        return column;
                    });
                })
                .then(res.response.ok)
                .fail(res.error);
        }
    );

    router.authenticated.post('/projects/:ns/:name/columns',
        container.get('authorization')('master'),
        function(req, res) {
            var column = req.body,
                createColumn = function(label) {
                    var timer = container.get('statsd').createTimer('redis.hexists');

                    return q.ninvoke(container.get('redis'), 'hexists', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns', column.title)
                        .then(function(col) {
                            timer.stop();

                            if (!!col) {
                                throw { code: 409, msg: 'Conflict'};
                            }

                            return {
                                title: column.title,
                                closable: !!column.closable,
                                canGoBackward: !!column.canGoBackward,
                                unpinned: !!column.unpinned,
                                position: column.position || 0,
                                limit: column.limit ? (column.limit < 0 ? 0 : parseInt(column.limit, 10)) : 0
                            };
                        })
                        .then(function(column) {
                            timer = container.get('statsd').createTimer('redis.hset');

                            return q.ninvoke(container.get('redis'), 'hset', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns', column.title, JSON.stringify(column))
                                .then(function() {
                                    timer.stop();

                                    column.color = label.color;

                                    return column;
                                })
                        })
                        .catch(res.error);
                };

            if (!column.title) {
                res.error({ message: 'Bad Request' }, 400);
            } else {
                container.get('gitlab.labels').all(req.user.private_token, req.params.ns, req.params.name)
                    .then(function(labels) {
                        var label;

                        if (labels) {
                            label = _.find(labels, { name: config.column_prefix + column.title.toLowerCase() });
                        }

                        return label;
                    })
                    .then(function(label) {
                        if (!label) {
                            label = {
                                name: config.column_prefix + column.title.toLowerCase(),
                                color: '#E5E5E5'
                            };

                            return container.get('gitlab.labels').persist(req.user.private_token, req.params.ns, req.params.name, label);
                        }

                        return label;
                    })
                    .then(createColumn)
                    .then(function(column) {
                        return container.get('notifier.columns').notifyNew(req.params.ns, req.params.name, column);
                    })
                    .then(res.response.created)
                    .catch(res.error);
            }
        }
    );

    router.authenticated.put('/projects/:ns/:name/columns/:column',
        container.get('authorization')('master'),
        function(req, res) {
            var timer = container.get('statsd').createTimer('redis.hget');

            q.ninvoke(container.get('redis'), 'hget', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns', req.params.column)
                .then(function(column) {
                    timer.stop();

                    if (!column) {
                        throw { code: 404, msg: 'Not found' };
                    }

                    column = JSON.parse(column);

                    if (typeof req.body.position !== "undefined") { column.position = req.body.position; }
                    if (typeof req.body.closable !== "undefined") { column.closable = req.body.closable; }
                    if (typeof req.body.canGoBackward !== "undefined") { column.canGoBackward = req.body.canGoBackward; }
                    if (typeof req.body.unpinned !== "undefined") { column.unpinned = req.body.unpinned; }
                    if (typeof req.body.limit !== "undefined") { column.limit = req.body.limit ? (req.body.limit < 0 ? 0 : parseInt(req.body.limit, 10)) : 0; }

                    timer = container.get('statsd').createTimer('redis.hset');

                    return q.ninvoke(container.get('redis'), 'hset', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns', req.params.column, JSON.stringify(column))
                        .then(function() {
                            timer.stop();

                            column.color = req.body.color;

                            container.get('notifier.columns').notifyEdit(req.params.ns, req.params.name, column);

                            res.response.ok(column);
                        });
                })
                .catch(res.error);
        }
    );

    router.authenticated.put('/projects/:ns/:name/columns/:column/move',
        container.get('authorization')('master'),
        function(req, res) {
            if (typeof req.body.position === 'undefined') {
                res.error.notAcceptable({
                    message: 'Not acceptable'
                });
            } else {
                var timer = container.get('statsd').createTimer('redis.hget');

                q.ninvoke(container.get('redis'), 'hget', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns', req.params.column)
                    .then(function(column) {
                        timer.stop();

                        if (!column) {
                            throw { code: 404, msg: 'Not found' };
                        }

                        column = JSON.parse(column);

                        var from = column.position,
                            to = req.body.position;

                        column.position = to;

                        timer = container.get('statsd').createTimer('redis.hset');

                        return q.ninvoke(container.get('redis'), 'hset', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns', req.params.column, JSON.stringify(column))
                            .then(function() {
                                timer.stop();

                                column.color = req.body.color;

                                container.get('notifier.columns').notifyMove(req.params.ns, req.params.name, column, from, to);

                                res.response.ok(column);
                            });
                    })
                    .catch(res.error);
            }
        }
    );

    router.authenticated.delete('/projects/:ns/:name/columns/:column',
        container.get('authorization')('master'),
        function(req, res) {
            var timer = container.get('statsd').createTimer('redis.hget');

            q.ninvoke(container.get('redis'), 'hget', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns', req.params.column)
                .then(function(column) {
                    timer.stop();

                    if (!column) {
                        throw { code: 404, msg: 'Not found' };
                    }

                    timer = container.get('statsd').createTimer('redis.hdel');

                    return q.ninvoke(container.get('redis'), 'hdel', 'laboard:' + req.params.ns + ':' + req.params.name + ':columns', req.params.column)
                        .then(function() {
                            timer.stop();

                            container.get('notifier.columns').notifyRemove(req.params.ns, req.params.name, column);

                            res.response.ok(JSON.parse(column));
                        });
                })
                .catch(res.error);
        }
    );
};
