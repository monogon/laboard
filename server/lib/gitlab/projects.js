var q = require('q'),
    _ = require('lodash'),
    projects = module.exports = function projects(client, formatter, container) {
        this.client = client;
        this.formatter = formatter;
        this.container = container;
    };

projects.prototype = {
    url: function(namespace, project, base) {
        var url = namespace;

        base = base || '/projects';

        if (!namespace && !project) {
            return base;
        }

        if (project) {
            url += '%2F' + project;
        }

        return base + '/' + url;
    },

    one: function(token, namespace, project) {
        var url = this.url(namespace, project),
            formatter = this.formatter,
            timerGet = this.container.get('statsd').createTimer('gitlab.get'),
            timerRoute = this.container.get('statsd').createTimer('gitlab.get.project');

        return this.client.get(token, url)
            .then(function(response) {
                timerGet.stop();
                timerRoute.stop();

                if (response.statusCode !== 200) {
                    throw resp;
                }

                return formatter.formatProjectFromGitlab(response.body);
            });
    },

    members: function(token, namespace, project) {
        var url = this.url(namespace, project),
            timerGet = this.container.get('statsd').createTimer('gitlab.get'),
            timerRoute = this.container.get('statsd').createTimer('gitlab.get.members');

        return q.all([
            this.client.get(token, url + '/members'),
            this.client.get(this.url(namespace, null, '/groups') + '/members'),
        ])
            .spread(function(project_response, group_response) {
                timerGet.stop();
                timerRoute.stop();

                if (project_response.statusCode !== 200) {
                    throw project_response;
                }

                if (group_response.statusCode !== 200) {
                    throw group_response;
                }

                return _.uniq(project_response.body.concat(group_response.body), 'id');
            });
    },

    all: function(token) {
        var url = this.url(),
            projects = [],
            formatter = this.formatter,
            client = this.client,
            timerFactory = this.container.get('statsd').createTimer.bind(this.container.get('statsd')),
            fetch = function(page) {
                var timerGet = timerFactory('gitlab.get'),
                    timerRoute = timerFactory('gitlab.get.projects'),
                    params = {};

                params.page = ++page;
                params.per_page = 50;

                return client.get(token, url, params)
                    .then(function(response) {
                        timerGet.stop();
                        timerRoute.stop();

                        if (response.statusCode !== 200) {
                            throw response;
                        }

                        projects = projects.concat(
                            response.body
                                .filter(function (project) { return !!project.issues_enabled; })
                                .map(formatter.formatProjectFromGitlab)
                        );

                        if (response.links.next) {
                            return fetch(page);
                        }

                        return projects;
                    });
            };

        return fetch(0);
    }
};
