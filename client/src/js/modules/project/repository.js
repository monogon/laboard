angular.module('laboard-frontend')
    .factory('ProjectsRepository', [
        '$rootScope', 'Restangular', '$q',
        function($root, $rest, $q) {
            var fetch = function (self) {
                    return $rest.all('projects')
                        .getList()
                        .then(function (projects) {
                            self.$objects = _.sortBy(projects, 'path_with_namespace');

                            return self.$objects;
                        });
                },
                fetchOne = function (id, self) {
                    return $rest.all('projects')
                        .one(id)
                        .get()
                        .then(function (column) {
                            self.add(column);

                            return column;
                        });
                },
                all = function (self) {
                    return fetch(self)
                        .then(function(columns) {
                            if (columns.length) {
                                self.all = function() {
                                    return allCached(self);
                                };
                            }

                            return columns;
                        });
                },
                allCached = function(self) {
                    return $q.when(self.$objects);
                },
                repository = {
                    $objects: null,

                    all: function() {
                        return all(this);
                    },
                    one: function(id) {
                        return fetchOne(id, this);
                    },

                    add: function (project) {
                        var added = false,
                            self = this;

                        this.$objects = this.$objects || [];

                        this.$objects.forEach(function(value, key) {
                            if (added) { return; }

                            if (value.path_with_namespace === project.path_with_namespace) {
                                self.$objects[key] = project;
                                added = true;
                            }
                        });

                        if (added === false && project.path_with_namespace) {
                            this.$objects.push(project);
                        }

                        return project;
                    },
                    clear: function() {
                        this.$objects = null;

                        this.all = function() {
                            return all(this);
                        };
                    },

                    members: function (project) {
                        return $rest.all('projects/' + project.path_with_namespace + '/members').getList();
                    },

                    labels: function (project) {
                        return $rest.all('projects/' + project.path_with_namespace + '/labels').getList();
                    }
                };

            return repository;
        }
    ]);
