(function(angular) {
    angular.module('laboard-frontend')
        .filter('slugify', [
            function() {
                return function(text) {
                    return text.replace(/[\W]+/g, '-');
                };
            }
        ]);
})(angular);
